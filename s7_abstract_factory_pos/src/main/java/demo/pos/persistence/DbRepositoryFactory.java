package demo.pos.persistence;

/**
 * @author Jan de Rijke.
 */
public class DbRepositoryFactory implements RepositoryFactory {
	private  SaleRepository saleRepository = new SaleDbRepository();
	private ProductRepository catalog = new ProductDescriptionDbRepository();

	@Override
	public SaleRepository getSaleRepository(){
		return saleRepository;
	}

	@Override
	public ProductRepository getProductRepository(){
		return catalog;
	}

}